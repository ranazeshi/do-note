package com.zsquare.do_note.repositories;


import androidx.lifecycle.MutableLiveData;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.ServerValue;
import com.zsquare.do_note.model.NicePlace;
import com.zsquare.do_note.model.Note;
import com.zsquare.do_note.utils.Constants;
import com.zsquare.do_note.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Singleton pattern
 */
public class NicePlaceRepository {

    interface onDataLoad{
        public MutableLiveData<List<Note>> onLoaded(ArrayList<Note> notes);
        public void onError(String Error);
    }
    private static NicePlaceRepository instance;
    private ArrayList<NicePlace> dataSet = new ArrayList<>();
    private ArrayList<Note> notes = new ArrayList<>();

    public static NicePlaceRepository getInstance(){
        if(instance == null){
            instance = new NicePlaceRepository();
        }
        return instance;
    }


    // Pretend to get data from a webservice or online source
    public MutableLiveData<List<NicePlace>> getNicePlaces(){
        setNicePlaces();
        MutableLiveData<List<NicePlace>> data = new MutableLiveData<>();
        data.setValue(dataSet);
        return data;
    }

    private void setNicePlaces(){
        dataSet.add(
                new NicePlace("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg",
                        "Havasu Falls")
        );
        dataSet.add(
                new NicePlace("https://i.redd.it/tpsnoz5bzo501.jpg",
                        "Trondheim")
        );
        dataSet.add(
                new NicePlace("https://i.redd.it/qn7f9oqu7o501.jpg",
                        "Portugal")
        );
        dataSet.add(
                new NicePlace("https://i.redd.it/j6myfqglup501.jpg",
                        "Rocky Mountain National Park")
        );
        dataSet.add(
                new NicePlace("https://i.redd.it/0h2gm1ix6p501.jpg",
                        "Havasu Falls")
        );
        dataSet.add(
                new NicePlace("https://i.redd.it/k98uzl68eh501.jpg",
                        "Mahahual")
        );
        dataSet.add(
                new NicePlace("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg",
                        "Frozen Lake")
        );
        dataSet.add(
                new NicePlace("https://i.redd.it/obx4zydshg601.jpg",
                        "Austrailia")
        );
    }


    // Pretend to get data from a webservice or online source
    public MutableLiveData<List<Note>> getNotes(String email){
        final MutableLiveData<List<Note>> data = new MutableLiveData<>();

        setNotes(email, new onDataLoad() {
            @Override
            public MutableLiveData<List<Note>> onLoaded(ArrayList<Note> notes) {
                data.setValue(notes);
                return data;
            }

            @Override
            public void onError(String Error) {

            }
        });
        data.setValue(notes);
        return data;
    }


    private void setNotes(String email, final onDataLoad onDataLoad) {
        HashMap<String, Object> timestampJoined = new HashMap<>();
        timestampJoined.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

//        notes.add(new Note("Note 1","this is my notes descriptions",timestampJoined));
//        notes.add(new Note("Note 2","this is my notes descriptions",timestampJoined));
//        notes.add(new Note("Note 3","this is my notes descriptions",timestampJoined));
//        notes.add(new Note("Note 4","this is my notes descriptions",timestampJoined));
//        notes.add(new Note("Note 5","this is my notes descriptions",timestampJoined));

        notes.clear();

        final String encodedEmail = Utils.encodeEmail(email.toLowerCase());
        //create an object of Firebase database and pass the the Firebase URL
        final Firebase userLocation = new Firebase(Constants.FIREBASE_URL_NOTES).child(encodedEmail).child("notes");

        userLocation.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshot) {
                                                            // do some stuff once
                                                            for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                                                                // TODO: handle the post
                                                                Note note = postSnapshot.getValue(Note.class);
                                                                notes.add(note);

                                                                android.util.Log.e("postSnapshot",note.getTitle());

                                                            }

                                                            onDataLoad.onLoaded(notes);
                                                        }

                                                        @Override
                                                        public void onCancelled(FirebaseError firebaseError) {
                                                            android.util.Log.e("firebaseError",firebaseError.getMessage());
                                                            onDataLoad.onError(firebaseError.getMessage());
                                                        }
                                                    });

//        userLocation.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
//                    // TODO: handle the post
//                    Note note = postSnapshot.getValue(Note.class);
//                    notes.add(note);
//                    android.util.Log.e("postSnapshot",note.getTitle());
//                }
//            }
//
//            @Override
//            public void onCancelled(FirebaseError firebaseError) {
//                android.util.Log.e("postSnapshotError",firebaseError.getMessage().toString());
//            }
//        });


    }

}











