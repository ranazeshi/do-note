package com.zsquare.do_note.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ServerValue;
import com.trncic.library.DottedProgressBar;
import com.zsquare.do_note.MainActivity;
import com.zsquare.do_note.ProfileActivity;
import com.zsquare.do_note.R;
import com.zsquare.do_note.model.User;
import com.zsquare.do_note.utils.Constants;
import com.zsquare.do_note.utils.SharedPrefManager;
import com.zsquare.do_note.utils.Utils;

import java.util.HashMap;



public class SplashScreenActivity extends AppCompatActivity {

    ImageView ivIcon;
    private static final String TAG = "AndroidClarified";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public static GoogleSignInAccount googleSignInAccount;
    private String idToken,name,email,photo;
    public SharedPrefManager sharedPrefManager;

    @Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount alreadyloggedAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (alreadyloggedAccount != null) {
//            Toast.makeText(this, "Already Logged In", Toast.LENGTH_SHORT).show();
            onLoggedIn(alreadyloggedAccount);
        } else {
//            Log.e(TAG, "Not logged in");
            goToLoginActivity();
        }


        if (mAuthListener != null){
            FirebaseAuth.getInstance().signOut();
        }
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Firebase.setAndroidContext(this);

        ivIcon=(ImageView)findViewById(R.id.ivIcon);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);// SDK21
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            }
        }

        final DottedProgressBar progressBar = (DottedProgressBar) findViewById(R.id.progress);
        progressBar.startProgress();

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                progressBar.stopProgress();
//                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
//                finish();
//            }
//        },2000);


        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();

        //this is where we start the Auth state Listener to listen for whether the user is signed in or not
        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // Get signedIn user
                FirebaseUser user = firebaseAuth.getCurrentUser();

                //if user is signed in, we call a helper method to save the user details to Firebase
                if (user != null) {
                    // User is signed in
                    createUserInFirebaseHelper();
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    private void createUserInFirebaseHelper(){

        try {
            //Since Firebase does not allow "." in the key name, we'll have to encode and change the "." to ","
            // using the encodeEmail method in class Utils
            final String encodedEmail = Utils.encodeEmail(email.toLowerCase());

            //create an object of Firebase database and pass the the Firebase URL
            final Firebase userLocation = new Firebase(Constants.FIREBASE_URL_USERS).child(encodedEmail);

            //Add a Listerner to that above location
            userLocation.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
                @Override
                public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {
                        /* Set raw version of date to the ServerValue.TIMESTAMP value and save into dateCreatedMap */
                        HashMap<String, Object> timestampJoined = new HashMap<>();
                        timestampJoined.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

                        // Insert into Firebase database
                        User newUser = new User(name, photo, encodedEmail, timestampJoined);
                        userLocation.setValue(newUser);

                        goToNotesActivity(googleSignInAccount);


                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                    Log.e(TAG, getString(R.string.log_error_occurred) + firebaseError.getMessage());
//                    goToLoginActivity();
                    //hideProgressDialog();
                    if (firebaseError.getCode() == FirebaseError.EMAIL_TAKEN) {

                    } else {

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
//            goToLoginActivity();
        }
    }



    @Override
    protected void onPause() {
        super.onPause();

        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    void goToNotesActivity(GoogleSignInAccount googleSignInAccount){

        Intent intent = new Intent(SplashScreenActivity.this, NoteListActivity.class);
        intent.putExtra(ProfileActivity.GOOGLE_ACCOUNT, googleSignInAccount);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    void goToLoginActivity(){

        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void onLoggedIn(GoogleSignInAccount googleSignInAccount) {

        idToken = googleSignInAccount.getIdToken();

        android.util.Log.e("idToken",""+idToken);

        name = googleSignInAccount.getDisplayName();
        email = googleSignInAccount.getEmail();
        Uri photoUri = googleSignInAccount.getPhotoUrl();
        photo = photoUri.toString();
        // Save Data to SharedPreference
        sharedPrefManager = new SharedPrefManager(SplashScreenActivity.this);
        sharedPrefManager.saveIsLoggedIn(SplashScreenActivity.this, true);

        sharedPrefManager.saveEmail(SplashScreenActivity.this, email);
        sharedPrefManager.saveName(SplashScreenActivity.this, name);
        sharedPrefManager.savePhoto(SplashScreenActivity.this, photo);

        sharedPrefManager.saveToken(SplashScreenActivity.this, idToken);
        //sharedPrefManager.saveIsLoggedIn(mContext, true);

        this.googleSignInAccount=googleSignInAccount;
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        firebaseAuthWithGoogle(credential);


    }

    //After a successful sign into Google, this method now authenticates the user with Firebase
    private void firebaseAuthWithGoogle(AuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential" + task.getException().getMessage());
                            task.getException().printStackTrace();
                            goToLoginActivity();
                        }else {
                            createUserInFirebaseHelper();
                            goToNotesActivity(googleSignInAccount);

                        }
                    }
                });
    }


}
