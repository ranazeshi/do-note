package com.zsquare.do_note.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zsquare.do_note.R;
import com.zsquare.do_note.adapters.RecyclerAdapter;
import com.zsquare.do_note.model.Note;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class NoteDetailsActivity extends AppCompatActivity {
    Note note;

    TextView tvNoteTitle,tvPublishDate, tvDescription;
    ImageView ivShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);
        note=(Note)getIntent().getExtras().getSerializable("note");
        if(note==null){
            finish();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvNoteTitle=findViewById(R.id.tvNoteTitle);
        tvPublishDate=findViewById(R.id.tvPublishDate);
        tvDescription=findViewById(R.id.tvDescription);

        tvNoteTitle.setText(note.getTitle());
        tvDescription.setText(note.getDescription());

        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' hh:mm:ss");
        Date date;
        try {
            date = new Date(Long.parseLong((note.getTimestampJoined().get("timestamp").toString())));
            System.out.println();

        }catch (NumberFormatException nfe){

            date=new Date(System.currentTimeMillis());
        }

        tvPublishDate.setText(formatter.format(date));


        ivShare=findViewById(R.id.ivShare);
        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareText(tvNoteTitle.getText().toString()+"\n\n"+tvDescription.getText().toString());
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id==android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void shareText(String text) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "MYTM News");
            i.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
