package com.zsquare.do_note.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.zsquare.do_note.MainActivity;
import com.zsquare.do_note.ProfileActivity;
import com.zsquare.do_note.R;
import com.zsquare.do_note.adapters.RecyclerAdapter;
import com.zsquare.do_note.model.NicePlace;
import com.zsquare.do_note.model.Note;
import com.zsquare.do_note.model.User;
import com.zsquare.do_note.utils.SharedPrefManager;
import com.zsquare.do_note.viewmodels.NoteListActivityViewModel;

import java.util.List;

public class NoteListActivity extends AppCompatActivity {

    public static final String GOOGLE_ACCOUNT = "google_account";
    private GoogleSignInAccount googleSignInAccount;

    private FirebaseAuth mAuth;


    private static final String TAG = "MainActivity";

    private FloatingActionButton mFab;
    private LinearLayout llPlaceHolder;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private ProgressBar mProgressBar;
    private NoteListActivityViewModel mMainActivityViewModel;

    public interface OnItemClickListener {
        void onItemClick(Note item);
    }

    LinearLayout layoutBottomSheet;

    BottomSheetBehavior sheetBehavior;

    TextView tvAddNote;
    EditText etAddNote,etAddNoteTitle;
    ImageView ivCloseBottomSheet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);
        mFab = findViewById(R.id.fab);
        llPlaceHolder=findViewById(R.id.llPlaceHolder);
        mRecyclerView = findViewById(R.id.recycler_view);
        mProgressBar = findViewById(R.id.progress_bar);

        googleSignInAccount = getIntent().getParcelableExtra(GOOGLE_ACCOUNT);

        mMainActivityViewModel = ViewModelProviders.of(this).get(NoteListActivityViewModel.class);

        mMainActivityViewModel.initNotes(googleSignInAccount.getEmail());

        mMainActivityViewModel.getNotes().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> nicePlaces) {
                mAdapter.notifyDataSetChanged();
                if(nicePlaces.size()>0){
                    llPlaceHolder.setVisibility(View.VISIBLE);
                }else{
                    llPlaceHolder.setVisibility(View.GONE);
                }

            }
        });

        mMainActivityViewModel.getIsUpdating().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean){
                    showProgressBar();
                }
                else{
                    hideProgressBar();
                    mAdapter.notifyDataSetChanged();
                    if(mMainActivityViewModel.getNotes().getValue().size()>0) {
                        mRecyclerView.smoothScrollToPosition(mMainActivityViewModel.getNotes().getValue().size() - 1);
                        llPlaceHolder.setVisibility(View.GONE);
                    }else{
                        llPlaceHolder.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                toggleBottomSheet();
            }
        });

        initRecyclerView();

        initBottomSheetAddNote();

    }

    private void initRecyclerView(){
        mAdapter = new RecyclerAdapter(this, mMainActivityViewModel.getNotes().getValue(), new OnItemClickListener() {
            @Override
            public void onItemClick(Note item) {
                gotoNoteDetails(item);
            }
        });
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
    }

    void gotoNoteDetails(Note note){
        Intent intent=new Intent(this,NoteDetailsActivity.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("note",note);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    void initBottomSheetAddNote(){

        layoutBottomSheet=findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        /**
         * bottom sheet state change listener we are changing button text when sheet changed state
         */
        sheetBehavior.setBottomSheetCallback(
                new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        switch (newState) {
                            case BottomSheetBehavior.STATE_HIDDEN:
                                break;
                            case BottomSheetBehavior.STATE_EXPANDED:
                            {
                            }
                            break;
                            case BottomSheetBehavior.STATE_COLLAPSED: { }
                            break;
                            case BottomSheetBehavior.STATE_DRAGGING:
                                break;
                            case BottomSheetBehavior.STATE_SETTLING:
                                break;
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
                });



        tvAddNote=findViewById(R.id.tvAddNote);
        tvAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Note note=new Note();
                note.setTitle(etAddNoteTitle.getText().toString());
                note.setDescription(etAddNote.getText().toString());
             mMainActivityViewModel.addNewNoteValue(note,googleSignInAccount.getEmail(),NoteListActivity.this);

             etAddNote.setText("");
             etAddNoteTitle.setText("");
             ivCloseBottomSheet.performClick();
             hideSoftKeyboard(etAddNote,NoteListActivity.this);
            }
        });

        etAddNoteTitle=findViewById(R.id.etAddNoteTitle);
        etAddNote=findViewById(R.id.etAddNote);
        ivCloseBottomSheet=findViewById(R.id.ivCloseBottomSheet);
        ivCloseBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextWatcher textWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()>1){
                    tvAddNote.setEnabled(true);
                }else{
                    tvAddNote.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };


        etAddNote.addTextChangedListener(textWatcher);

    }


    public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


    @Override
    public void onBackPressed() {
        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            return;
        }
        super.onBackPressed();
    }

    public static void hideSoftKeyboard(View v, Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.action_logout){
            signOut();
        }
        return super.onOptionsItemSelected(item);
    }


    private void signOut(){
        new SharedPrefManager(this).clear();

        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
        mAuth.signOut();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.web_client_id))
                .requestEmail()
                .build();

        GoogleSignInClient googleSignInClient = (GoogleSignInClient) GoogleSignIn.getClient(this, gso);

        googleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {


                Intent intent=new Intent(NoteListActivity.this,MainActivity.class);
                intent.putExtra(ProfileActivity.GOOGLE_ACCOUNT, googleSignInAccount);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

    }

}




















