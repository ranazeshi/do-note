package com.zsquare.do_note.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zsquare.do_note.R;
import com.zsquare.do_note.activities.NoteListActivity;
import com.zsquare.do_note.model.NicePlace;
import com.zsquare.do_note.model.Note;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Note> mNicePlaces = new ArrayList<>();
    private Context mContext;
    private NoteListActivity.OnItemClickListener onItemClickListener;

    public RecyclerAdapter(Context context, List<Note> nicePlaces, NoteListActivity.OnItemClickListener onItemClickListener) {
        mNicePlaces = nicePlaces;
        mContext = context;
        this.onItemClickListener=onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        // Set the name of the 'NicePlace'
        ((ViewHolder)viewHolder).tvNoteTitle.setText(mNicePlaces.get(i).getTitle());
        ((ViewHolder)viewHolder).tvDescription.setText(mNicePlaces.get(i).getDescription());

        ((ViewHolder)viewHolder).bind(mNicePlaces.get(i),onItemClickListener);

        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' hh:mm:ss");
        Date date;
        if(mNicePlaces.get(i).getTimestampJoined()!=null) {
            try {
                date = new Date(Long.parseLong((mNicePlaces.get(i).getTimestampJoined().get("timestamp").toString())));
                System.out.println();

            } catch (NumberFormatException nfe) {

                date = new Date(System.currentTimeMillis());
            }
        }else{
            date = new Date(System.currentTimeMillis());
        }
        ((ViewHolder) viewHolder).tvPublishDate.setText(formatter.format(date));

    }

    @Override
    public int getItemCount() {
        return mNicePlaces.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvNoteTitle,tvPublishDate,tvDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNoteTitle=itemView.findViewById(R.id.tvNoteTitle);
            tvPublishDate=itemView.findViewById(R.id.tvPublishDate);
            tvDescription=itemView.findViewById(R.id.tvDescription);


        }

        public void bind(final Note item, final NoteListActivity.OnItemClickListener listener) {
            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.onItemClick(item);
                        }
                    });
        }
    }
}
