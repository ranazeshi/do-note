package com.zsquare.do_note.utils;

/**
 * Created by Gino Osahon on 04/03/2017.
 */
public class Constants {

    public static final String FIREBASE_URL = "https://do-note-38275.firebaseio.com/";
    public static final String FIREBASE_LOCATION_USERS = "users";
    public static final String FIREBASE_URL_USERS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USERS;
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "timestamp";


    public static final String FIREBASE_LOCATION_NOTES = "notes";
    public static final String FIREBASE_URL_NOTES = FIREBASE_URL + "/" + FIREBASE_LOCATION_NOTES;

}
