package com.zsquare.do_note.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Gino Osahon on 04/03/2017.
 */

public class Note implements Serializable {

    private String title;
    private String description;
    private HashMap<String, Object> timestampJoined;

    public Note() {
    }

    public Note(String title, String description, HashMap<String, Object> timestampJoined) {
        this.title = title;
        this.description = description;
        this.timestampJoined = timestampJoined;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTimestampJoined(HashMap<String, Object> timestampJoined) {
        this.timestampJoined = timestampJoined;
    }

    public HashMap<String, Object> getTimestampJoined() {
        return timestampJoined;
    }
}
