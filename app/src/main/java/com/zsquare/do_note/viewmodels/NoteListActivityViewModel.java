package com.zsquare.do_note.viewmodels;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.ServerValue;
import com.zsquare.do_note.MainActivity;
import com.zsquare.do_note.R;
import com.zsquare.do_note.model.NicePlace;
import com.zsquare.do_note.model.Note;
import com.zsquare.do_note.model.User;
import com.zsquare.do_note.repositories.NicePlaceRepository;
import com.zsquare.do_note.utils.Constants;
import com.zsquare.do_note.utils.Utils;

import java.util.HashMap;
import java.util.List;

public class NoteListActivityViewModel extends ViewModel {

    private MutableLiveData<List<NicePlace>> mNicePlaces;
    private MutableLiveData<List<Note>> Notes;
    private NicePlaceRepository mRepo;
    private MutableLiveData<Boolean> mIsUpdating = new MutableLiveData<>();

    public void init(){
        if(mNicePlaces != null){
            return;
        }else {
            mRepo = NicePlaceRepository.getInstance();
            mNicePlaces = mRepo.getNicePlaces();
        }
    }

    public void initNotes(String email){
        if(Notes != null){
            return;
        }else {
            mRepo = NicePlaceRepository.getInstance();
            mIsUpdating.setValue(true);
            Notes = mRepo.getNotes(email);
            mIsUpdating.setValue(false);


        }
    }

    public void addNewValue(final NicePlace nicePlace){
        mIsUpdating.setValue(true);

        new AsyncTask<Void, Void, Void>(){
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                List<NicePlace> currentPlaces = mNicePlaces.getValue();
                currentPlaces.add(nicePlace);
                mNicePlaces.postValue(currentPlaces);
                mIsUpdating.postValue(false);


            }

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public void addNewNoteValue(Note note, String email, final Context context) {
        mIsUpdating.setValue(true);

        createNoteInFirebaseHelper(note,email,context);

    }
        private void createNoteInFirebaseHelper(final Note note, String email, final Context context){

        //Since Firebase does not allow "." in the key name, we'll have to encode and change the "." to ","
        // using the encodeEmail method in class Utils
        final String encodedEmail = Utils.encodeEmail(email.toLowerCase());

        //create an object of Firebase database and pass the the Firebase URL
        final Firebase userLocation = new Firebase(Constants.FIREBASE_URL_NOTES).child(encodedEmail);

        //Add a Listerner to that above location
        userLocation.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
//                if (dataSnapshot.getValue() == null){
                    /* Set raw version of date to the ServerValue.TIMESTAMP value and save into dateCreatedMap */

                HashMap<String, Object> timestampJoined = new HashMap<>();
                    timestampJoined.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

                    // Insert into Firebase database
                    Note newUser = new Note(note.getTitle(),note.getDescription(), timestampJoined);

                note.setTimestampJoined(timestampJoined);

                userLocation.getRef().child("notes").push().setValue(newUser);

                List<Note> currentPlaces = Notes.getValue();
                currentPlaces.add(newUser);
                Notes.postValue(currentPlaces);

                Toast.makeText(context, "Note Added!", Toast.LENGTH_SHORT).show();
                mIsUpdating.setValue(false);

//                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

                mIsUpdating.setValue(false);
                Toast.makeText(context, ""+firebaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public LiveData<List<NicePlace>> getNicePlaces(){
        return mNicePlaces;
    }

    public LiveData<List<Note>> getNotes(){
        return Notes;
    }


    public LiveData<Boolean> getIsUpdating(){
        return mIsUpdating;
    }
}
